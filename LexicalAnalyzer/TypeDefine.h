#pragma once
#include"stdafx.h"

enum TOKENTYPE
{
	NONE = -1,
	//常量
	MS_ID = 0, MS_INT = 1, MS_FLOAT = 2, MS_STRING = 3, MS_CHAR = 4,

	//保留字，数据类型
	DT_INT = 5, DT_FLOAT = 6, DT_CHAR = 7, DT_VOID = 8,

	//计算符
	OP_PLUS = 9, OP_MINUS = 10, OP_MULTIPLY = 11, OP_DIVIDE = 12, OP_ASSIGN = 13,
	OP_OR = 14, OP_AND = 15, OP_UNEQUAL = 16, OP_EQUAL = 17, OP_MORETHAN = 18,
	OP_LESSTHAN = 19,

	//界符
	SP_LEFTBRACKET = 20, SP_RIGHTBRACKET = 21, SP_OPENBRACK = 22,
	SP_CLOSEBRACK = 23, SP_SEMICOLON = 24, SP_LEFTPARENTHESIS = 25,
	SP_RIGHTPARENTHESIS = 26,

	//保留字
	ST_IF = 27, ST_ELSE = 28, ST_WHILE = 29, ST_FOR = 30, ST_BREAK = 31,
	ST_CONTINUE = 32, ST_RETURN = 33, ST_PRINTF = 34,
};

class TOKEN
{
public:
	TOKEN();
	TOKEN(int, int, TOKENTYPE, string);
	~TOKEN();

	void SaveResult();

public:
	int rowNum;//行号
	int colNum;//列号
	TOKENTYPE type;//单词类型
	string val;//值
	union {
		char charval;//字符
		int intval;//整数
		double floatval;//浮点数
	}value;
};




#pragma once
#include"stdafx.h"
#include"TypeDefine.h"

class CScanner
{
public:
	CScanner();
	CScanner(const char* srcFile);
	~CScanner();

	bool ReadSourceFile();//读取源文件
	bool IsEof();//是否到文件结尾

	TOKEN NextToken();
	char NextChar();
	void BackChar();
	char NextValidChar();
	void PrintToken();//将识别的字符输出到控制台
	void SaveToken();//保存单词符号到m_curToken
	bool InsertId(TOKEN sToken);//插入一个标识符
	bool InsertConst(TOKEN sToken);//插入一个常数

	//状态节点处理子函数
	bool IsSeparatorOperator(char ch);//是否是分隔符
	bool IsArithmeticOperator(char ch);//是否为算术运算符
	bool IsRelationalOperator(char ch);//是否是关系运算符
	bool IsLogicOperator(char ch);//是否为逻辑运算符
	bool IsLetter(char ch);//判断单词是否为字母
	bool IsDigit(char ch);//判断单词是否为数字
	bool ToNumState();//常数识别
	bool ToIndentifyState();//标识符识别
	bool ToStringState();//判断字符串子函数
	bool ToCharState();//判断字符子函数
	bool ToRealState();//判断实数
	bool ToCommentState();//判断是否为注释
	bool ToUnequalState();//判断不等符号子函数
	bool ToOrState();//判断或符号子函数
	bool ToAndState();//判断与符号子函数
	bool ToEqualState();//是否等于等号
	bool ToErrorState();//异常处理
	bool ToAcceptState(TOKENTYPE eType, bool bBack = false); //接受一个识别出的单词符号
	TOKENTYPE keyWord();//识别是否为保留字

	string GetTokenString();//获取单词的字符串值
	int Compare(string str1, string str2);//比较两个字符串


private:
	char* m_srcFileName;//源文件名
	char* m_pCache;//缓存
	int m_nBegin;//标识Token的开头
	int m_nForward;//标识当前正在处理的字符
	TOKEN m_curToken;//当前单词符号类型
	map<string, TOKENTYPE>m_mReserved;//保留字表
	bool m_bEof;//文件结尾标志
};


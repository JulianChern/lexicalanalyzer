#include "CScanner.h"

CScanner::CScanner()
{
	m_srcFileName = 0;
	m_pCache = 0;
	m_nBegin = 0;
	m_nForward = -1;
	m_bEof = false;
}

CScanner::CScanner(const char* srcFile) {
	m_pCache = 0;
	m_nBegin = 0;
	m_nForward = -1;
	m_bEof = false;
	if (srcFile != nullptr) {
		m_srcFileName = new char[strlen(srcFile) + 1];
		strcpy(m_srcFileName, srcFile);
	}
	else
		m_srcFileName = 0;
	ReadSourceFile();//读取源文件
	//设置保留字
	char aReserved[12][10] = { "int","float","char","void","if","else" ,"while","for","break","continue","return","printf" };
	TOKENTYPE asTypes[12] = { DT_INT,DT_FLOAT,DT_CHAR,DT_VOID,ST_IF,ST_ELSE,ST_WHILE,ST_FOR,ST_BREAK,ST_CONTINUE,ST_RETURN,ST_PRINTF };
	for (int i = 0; i < 12; i++)
	{
		m_mReserved.insert(pair<string, TOKENTYPE>(aReserved[i], asTypes[i]));
	}
}

CScanner::~CScanner()
{
	delete[]m_srcFileName;
	delete[]m_pCache;
}

//读取源文件
bool CScanner::ReadSourceFile() {
	ifstream ifile;
	//	char ch;
	ifile.open(m_srcFileName, ios::in);
	if (!ifile.is_open()) {
		cout << "文件打开失败" << endl;
		exit(1);
	}
	//获取文件长度
	ifile.seekg(0, ios::end);
	streamoff nLength = ifile.tellg();
	if (m_pCache != nullptr) {
		delete[]m_pCache;
		m_pCache = nullptr;
	}
	m_pCache = new char[(unsigned int)nLength + 2];
	memset(m_pCache, NULL, sizeof(char)*((unsigned int)nLength + 2));
	ifile.clear();
	ifile.seekg(0, ios::beg);
	/*ch = ifile.get();
	strcpy(m_pCache, &ch);
	while (ifile.get(ch)) {
		strcat(m_pCache, &ch);
	}*/
	ifile.read(m_pCache, nLength);
	ifile.close();
	return true;
}

bool CScanner::IsEof() {
	return m_bEof;
}

//判断单词是否为数字
bool CScanner::IsDigit(char ch) {
	if (ch >= '0'&&ch <= '9')
		return true;
	else
		return false;
}

//判断单词是否为字母
bool CScanner::IsLetter(char ch) {
	if (ch >= 'a'&&ch <= 'z' || ch >= 'A'&&ch <= 'Z')
		return true;
	else
		return false;
}

//读取下一个待处理的字符
char CScanner::NextChar() {
	m_nForward++;
	return m_pCache[m_nForward];
}

//回退一个字符
void CScanner::BackChar() {
	m_nForward--;
}

//读取下一个有效非空字符
char CScanner::NextValidChar() {
	char	ch = NextChar();
	while (!((ch > ' ' && ch < 127) || ch == '\0')) {
		ch = NextChar();
	}
	return ch;
}

//返回读取到的单词符号
TOKEN CScanner::NextToken() {
	memset(&m_curToken, NULL, sizeof(TOKEN));
	m_curToken.type = NONE;

	char ch = NextValidChar();
	m_nBegin = m_nForward;
	if (ch == '\0')
		m_bEof = true;
	else if (IsDigit(ch))
		ToNumState();
	else if (IsLetter(ch))
		ToIndentifyState();
	else if (ch == '\"')
		ToStringState();
	else if (ch == '\'')
		ToCharState();
	else if (IsSeparatorOperator(ch));
	else if (IsRelationalOperator(ch));
	else if (IsLogicOperator(ch));
	else if (IsArithmeticOperator(ch));
	else
		ToErrorState();

	return m_curToken;
}


//判断是否为分隔符
bool CScanner::IsSeparatorOperator(char ch) {
	switch (ch)
	{
	case '(': {
		return ToAcceptState(SP_LEFTBRACKET); break;
	}
	case')': {
		return ToAcceptState(SP_RIGHTBRACKET); break;
	}
	case '[': {
		return ToAcceptState(SP_LEFTPARENTHESIS); break;
	}
	case ']': {
		return ToAcceptState(SP_RIGHTPARENTHESIS); break;
	}
	case '{': {
		return ToAcceptState(SP_OPENBRACK); break;
	}
	case '}': {
		return ToAcceptState(SP_CLOSEBRACK); break;
	}
	case ';': {
		return ToAcceptState(SP_SEMICOLON); break;
	}
	default: {
		return false; break;
	}
	}
}

//判断是否为关系运算符
bool CScanner::IsRelationalOperator(char ch) {
	switch (ch)
	{
	case '<': {
		return ToAcceptState(OP_LESSTHAN); break;
	}
	case '>': {
		return ToAcceptState(OP_MORETHAN); break;
	}
	case '!': {
		return ToUnequalState(); break;
	}
	case '=': {
		return ToEqualState(); break;
	}
	default: {
		return false; break;
	}
	}
}

//是否为算术运算符
bool CScanner::IsArithmeticOperator(char ch) {
	switch (ch)
	{
	case '+': {
		return ToAcceptState(OP_PLUS); break;
	}
	case '-': {
		return ToAcceptState(OP_MINUS); break;
	}
	case '*': {
		return ToAcceptState(OP_MULTIPLY); break;
	}
	case '/': {
		return ToCommentState(); break;
	}
	default: {
		return false;
		break;
	}
	}
}

//是否为逻辑运算符
bool CScanner::IsLogicOperator(char ch) {
	switch (ch)
	{
	case '|': {
		return ToOrState(); break;
	}
	case '&': {
		return ToAndState(); break;
	}
	default:
		return false;
		break;
	}
}

//接受一个识别出的单词符号
bool CScanner::ToAcceptState(TOKENTYPE eType, bool bBack) {
	if (bBack == true)
		BackChar();
	m_curToken.type = eType;
	SaveToken();
	return true;
}

//判断是否为注释或者除号
bool CScanner::ToCommentState() {
	char ch = NextChar();
	if (ch == '/') {
		while (ch != '\n')//将"//"注释后面所有的内容全部清除
			ch = NextChar();
		return ToAcceptState(NONE);
	}
	else if (ch == '*') {
		ch = NextChar();
		while (!(ch == '*'&&NextChar() == '/'))//将"//"注释后面所有的内容全部清除
			ch = NextChar();
		return ToAcceptState(NONE);
	}
	else
		return ToAcceptState(OP_DIVIDE, true);

}

//是否等于等号
bool CScanner::ToEqualState() {
	char ch = NextChar();
	if (ch == '=')
		return ToAcceptState(OP_EQUAL);
	else
		return ToAcceptState(OP_ASSIGN, true);
}

//判断不等符号子函数
bool CScanner::ToUnequalState()
{
	char ch = NextChar();
	if (ch == '=')
		return ToAcceptState(OP_UNEQUAL);
	else
		return ToErrorState();
}

//判断字符串子函数
bool CScanner::ToStringState() {
	char ch = NextChar();
	while (ch != '\"')
		ch = NextChar();
	return ToAcceptState(MS_STRING);
}

//判断字符子函数
bool CScanner::ToCharState() {
	NextChar();
	if (NextChar() == '\'')
		return ToAcceptState(MS_CHAR);
	else
		return ToErrorState();
}

//判断或符号子函数
bool CScanner::ToOrState()
{
	char ch = NextChar();
	if (ch == '|')
		return ToAcceptState(OP_OR);
	else
		return ToErrorState();
}

//判断与符号子函数
bool CScanner::ToAndState()
{
	char ch = NextChar();
	if (ch == '&')
		return ToAcceptState(OP_AND);
	else
		return ToErrorState();
}

//常数识别
bool CScanner::ToNumState() {
	char ch = NextChar();
	while (IsDigit(ch) == true)
		ch = NextChar();
	if (ch == '.')
		return ToRealState();
	else
		return ToAcceptState(MS_INT, true);

}

//判断实数
bool CScanner::ToRealState() {
	char ch = NextChar();
	while (IsDigit(ch) == true)
		ch = NextChar();
	if (ch == '.')
		return ToErrorState();
	else if (ch == 'E' || ch == 'e') {
		ch = NextChar();
		if (ch == '+' || ch == '-' || IsDigit(ch)) {
			ch = NextChar();
			while (IsDigit(ch) == true)
				ch = NextChar();
			if (ch == '.')
				return ToErrorState();
			else
				return ToAcceptState(MS_FLOAT, true);
		}
		else
			return ToErrorState();
	}
	else
		return ToAcceptState(MS_FLOAT, true);
	return ToErrorState();
}

//标识符识别
bool CScanner::ToIndentifyState() {
	char ch = NextChar();
	while (IsDigit(ch) || IsLetter(ch))
		ch = NextChar();
	BackChar();
	TOKENTYPE eType = keyWord();
	if (eType == MS_ID)
		InsertId(m_curToken);
	ToAcceptState(eType);
	return true;
}

//异常处理
bool CScanner::ToErrorState() {
	cout << "ERROR" << " " << m_nBegin << " ";
	PrintToken();
	return false;
}

//将识别的字符输出到控制台
void CScanner::PrintToken() {
	for (int i = m_nBegin; i <= m_nForward; i++)
		cout << m_pCache[i];
	cout << endl;
}

//保存单词符号到m_curToken
void CScanner::SaveToken() {
	m_curToken.val = GetTokenString();
	switch (m_curToken.type)
	{
	case MS_INT: {//整数
		//验证是否越界
		string strToken = m_curToken.val;
		string strMax;
		stringstream ss;
		ss << numeric_limits<int>::max();
		ss >> strMax;

		if (Compare(strToken, strMax) == 1) {
			ToErrorState();
		}
		else
			m_curToken.value.intval = std::atoi(strToken.c_str());
		break;
	}
	case MS_FLOAT: {//浮点数
		//验证实数是否越界
		string strToken = m_curToken.val;
		double dbValue = std::atof(strToken.c_str());
		if (dbValue<numeric_limits<double>::min() || dbValue>numeric_limits<double>::max() || dbValue == 0)
			ToErrorState();
		else
			m_curToken.value.floatval = dbValue;
		break;
	}
	case MS_CHAR: {
		//验证字符串是否越界
		m_curToken.value.charval = m_pCache[m_nBegin + 1];
		break;
	}
	default:
		break;
	}
}

//识别是否为保留字
TOKENTYPE CScanner::keyWord() {
	m_curToken.type = MS_ID;
	//拷贝Token
	int nLength = m_nForward - m_nBegin + 1;
	char* buf = new char[nLength + 1];
	memset(buf, NULL, sizeof(char)*(nLength + 1));
	for (int i = 0; i < nLength; i++)
		*(buf + i) = m_pCache[m_nBegin + i];
	//判断是否为保留字
	bool bReserved = false;
	map<string, TOKENTYPE>::iterator l_it;
	l_it = m_mReserved.find(buf);
	if (l_it != m_mReserved.end()) {
		bReserved = true;
		m_curToken.type = l_it->second;
	}
	delete[]buf;
	return m_curToken.type;
}

//获取单词的字符串值
string CScanner::GetTokenString() {
	char* pVal = new char[m_nForward - m_nBegin + 2];
	memset(pVal, NULL, sizeof(char)*(m_nForward - m_nBegin + 2));
	for (int i = m_nBegin; i <= m_nForward; i++)
		*(pVal + i - m_nBegin) = m_pCache[i];
	string strval = pVal;
	delete[]pVal;
	pVal = NULL;
	return strval;
}

//比较整数大小，越界返回1
int CScanner::Compare(string str1, string str2) {
	int lLen = strlen(str1.c_str());
	int rLen = strlen(str2.c_str());
	if (lLen > rLen)
		return 1;
	else if (lLen == rLen)
		return str1 > str2;
	else
		return 0;
}

//插入一个标识符
bool CScanner::InsertId(TOKEN sToken) {
	return true;
}

//插入一个常数
bool CScanner::InsertConst(TOKEN sToken) {
	return true;
}


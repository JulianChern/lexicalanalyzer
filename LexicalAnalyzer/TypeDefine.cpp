#include"TypeDefine.h"

TOKEN::TOKEN()
{
	rowNum = 0;
	colNum = 0;
	type = NONE;
	val = "";
}

TOKEN::TOKEN(int rNum, int cNum, TOKENTYPE tType, string tVal) {
	rowNum = rNum;
	colNum = cNum;
	type = tType;
	val = tVal;
}

TOKEN::~TOKEN()
{
}

void TOKEN::SaveResult() {
	ofstream ofile;
	ofile.open("res\\tokens.txt", ios::out | ios::app);
	if (!ofile.is_open()) {
		cout << "�ļ���ʧ��!" << endl;
		exit(1);
	}
	ofile << rowNum << " ";
	ofile << colNum << " ";
	ofile << type << " ";
	ofile << val << " " << endl;

	ofile.close();
}

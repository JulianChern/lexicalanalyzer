// LexicalAnalyzer.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include"CScanner.h"


int main() {
	//创建单词符号输出文件
	ofstream ofile;
	ofile.open("res\\tokens.txt", ios::out | ios::trunc);
	//定义一个扫描器
	char srcFName[] = "res\\test.cpp";
	CScanner scanner(srcFName);
	int k = 0;
	while (!scanner.IsEof()) {
		TOKEN token = scanner.NextToken();
		if (token.type != NONE) {
			ofile << setfill('#') << setiosflags(ios::right) << setw(3) << token.type << "\t";
			ofile << setiosflags(ios::left) << token.val << endl;
		}
	}
	ofile.close();

    return 0;
}

